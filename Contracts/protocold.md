event SupplyReceived(address account, address asset, uint amount, uint startingBalance, uint newBalance);
event SupplyWithdrawn(address account, address asset, uint amount, uint startingBalance, uint newBalance);
event BorrowTaken(address account, address asset, uint amount, uint startingBalance, uint borrowAmountWithFee, uint newBalance);
event BorrowRepaid(address account, address asset, uint amount, uint startingBalance, uint newBalance);
event BorrowLiquidated(address targetAccount,
        address assetBorrow,
        uint borrowBalanceBefore,
        uint borrowBalanceAccumulated,
        uint amountRepaid,
        uint borrowBalanceAfter,
        address liquidator,
        address assetCollateral,
        uint collateralBalanceBefore,
        uint collateralBalanceAccumulated,
        uint amountSeized,
        uint collateralBalanceAfter);

event SupportedMarket(address asset, address interestRateModel);
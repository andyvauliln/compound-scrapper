using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Web3;

namespace compound
{
    public class EthereumService: IEthereumService
    {
        protected Nethereum.JsonRpc.Client.IClient EthLogsProvider { get; }

		private readonly ILogger<EthereumService> _logger;
        private readonly IConfigurationRoot _config;

		protected string CompoundContractAddress { get; }
		protected string CompoundContractAbi { get; }

        public EthereumService(ILogger<EthereumService> logger, IConfigurationRoot config)
        {
            _logger = logger;
            _config = config;

            CompoundContractAddress = config["services:ethereum:compoundContractAddress"];
			CompoundContractAbi = config["services:ethereum:compoundContractAbi"];


			EthLogsProvider = new Nethereum.JsonRpc.Client.RpcClient(new Uri(config["services:ethereum:logsProvider"]));
        }

        public async Task<BorrowTakenEvents> GetBorrowTakenEvents(BigInteger from, BigInteger to, BigInteger confirmationsRequired) {

			var web3 = new Web3(EthLogsProvider);

			var contract = web3.Eth.GetContract(
				CompoundContractAbi,
				CompoundContractAddress
			);

			HexBigInteger hexLaxtestBlock;
			var syncResp = await web3.Eth.Syncing.SendRequestAsync();
			if (syncResp.IsSyncing) {
				hexLaxtestBlock = syncResp.CurrentBlock;
			}
			else {
				hexLaxtestBlock = await web3.Eth.Blocks.GetBlockNumber.SendRequestAsync();
			}

			var latestConfirmedBlock = hexLaxtestBlock.Value -= confirmationsRequired;

			var hexFromBlock = new HexBigInteger(BigInteger.Min(from, latestConfirmedBlock));
			var hexToBlock = new HexBigInteger(BigInteger.Min(to, latestConfirmedBlock));

			var evnt = contract.GetEvent("SupportedMarket");

			var filter1 = evnt.CreateFilterInput();
			

			// var filter = await evnt.CreateFilterBlockRangeAsync(
			// 	new BlockParameter(hexFromBlock),
			// 	new BlockParameter(hexToBlock)
			// );

			var events = new List<CompoundRequest>();
			var logs = await evnt.GetAllChanges<SupportedMarket>(filter1);
			//var logs = await evnt.GetAllChanges<BorrowTakenMapping>(filter1);

			// foreach (var v in logs) {
			// 	if (!v.Log.Removed) {
			// 		events.Add(new CompoundRequest() {
			// 			Address = v.Event.Account,
			// 			Asset = v.Event.Asset,
			// 			Amount = v.Event.Amount,
			// 			StartingBalance = v.Event.StartingBalance,
			// 			BorrowAmountWithFee = v.Event.BorrowAmountWithFee,
			// 			NewBalance = v.Event.NewBalance,
			// 		});
			// 	}
			// }

			return new BorrowTakenEvents() {

			};
		}
		internal class BorrowTakenMapping {

			[Parameter("address", "account", 1, false)]
			public string Account { get; set; }

			[Parameter("address", "asset", 2, false)]
			public string Asset { get; set; }

			[Parameter("uint", "amount", 3, false)]
			public BigInteger Amount { get; set; }

			[Parameter("uint", "startingBalance", 4, false)]
			public BigInteger StartingBalance { get; set; }

			[Parameter("uint", "borrowAmountWithFee", 5, true)]
			public BigInteger BorrowAmountWithFee { get; set; }

			[Parameter("uint", "newBalance", 6, true)]
			public BigInteger NewBalance { get; set; }
		}

		[Event("SupportedMarket")]
		internal class SupportedMarket
		{
			[Parameter("address", "account", 1, false)]
			public string Account { get; set; }

			[Parameter("address", "asset", 2, false)]
			public string Asset { get; set; }
		}
    }
}
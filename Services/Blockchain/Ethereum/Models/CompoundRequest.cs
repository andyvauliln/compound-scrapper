using System.Numerics;

namespace compound
{
    public sealed class CompoundRequest
    {
        /// <summary>
		/// User address
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// Asset address
		/// </summary>
		public string Asset { get; set; }

		/// <summary>
		/// Starting Balance
		/// </summary>
		public BigInteger StartingBalance { get; set; }

		/// <summary>
		/// Amount (input amount)
		/// </summary>
		public BigInteger Amount { get; set; }

		/// <summary>
		/// Borrow Amount With Fee
		/// </summary>
		public BigInteger BorrowAmountWithFee { get; set; }
		
        /// <summary>
		/// New Balance
		/// </summary>
		public BigInteger NewBalance { get; set; }
		
		// ---

		/// <summary>
		/// Block number
		/// </summary>
		public BigInteger BlockNumber { get; set; }

		/// <summary>
		/// Transaction ID
		/// </summary>
		public string TransactionId { get; set; }
    }
}

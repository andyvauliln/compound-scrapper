using System.Numerics;
using System.Threading.Tasks;
using compound;

public interface IEthereumService
{
	/// <summary>
	/// Get BorrowTaken events
	/// </summary>
	 Task<BorrowTakenEvents> GetBorrowTakenEvents(BigInteger from, BigInteger to, BigInteger confirmationsRequired); 
}
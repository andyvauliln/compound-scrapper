using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Example.Proto;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace compound
{
    public interface IProduserService
    {
        Task Run();
    }
   public class KafkaProduserService : IProduserService
    {
     
		private readonly ILogger<KafkaProduserService> _logger;
        private readonly IConfigurationRoot _config;

		protected string CompoundContractAddress { get; }
		protected string CompoundContractAbi { get; }

        public KafkaProduserService(ILogger<KafkaProduserService> logger, IConfigurationRoot config)
        {
            _logger = logger;
            _config = config;

        }
        public async Task Run()
        {
            var config = new Dictionary<string, object> { { "bootstrap.servers", "localhost:9092" } };
            var topicName = "foo";

            using (var p = new Producer<Null, LogMsg>(config, null, new ProtoSerializer<LogMsg>()))
            {
                var cancelled = false;

                while (!cancelled)
                {
                    
                    var value = new LogMsg
                    {
                        IP = "127.0.0.1",
                        Message = "val",
                        Severity = Severity.Info
                    };
       
                    var deliveryReport = p.ProduceAsync(topicName, null, value).Result;
                    _logger.LogDebug(
                        deliveryReport.Error.Code == ErrorCode.NoError
                            ? $"delivered to: {deliveryReport.TopicPartitionOffset}"
                            : $"failed to deliver message: {deliveryReport.Error.Reason}"
                    );
                }
            }
        }
    }
}
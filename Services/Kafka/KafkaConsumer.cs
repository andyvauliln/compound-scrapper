using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Example.Proto;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace compound
{
    public interface IConsumerService
    {
        Task Run();
    }

    internal class KafkaConsumerService : IConsumerService
    {
        private readonly ILogger<KafkaConsumerService> _logger;
        private readonly IConfigurationRoot _config;

        public KafkaConsumerService(ILogger<KafkaConsumerService> logger, IConfigurationRoot config)
        {
            _logger = logger;
            _config = config;
        }
        public async Task Run()
        {
            var cconfig = new Dictionary<string, object>
            {
                { "bootstrap.servers", "localhost:9092" },
                { "group.id", Guid.NewGuid().ToString() }
            };
            var topicName = "foo";

            using (var consumer = new Consumer<Null, LogMsg>(cconfig, null, new ProtoDeserializer<LogMsg>()))
            {
                consumer.OnMessage += (_, msg)
                    => _logger.LogDebug($"Topic: {msg.Topic} Partition: {msg.Partition} Offset: {msg.Offset} {msg.Value}");

                consumer.Subscribe(topicName);
                Console.WriteLine($"Subscribed to: [{string.Join(", ", consumer.Subscription)}]");

                var cancelled = false;
                Console.CancelKeyPress += (_, e) =>
                {
                    e.Cancel = true;  // prevent the process from terminating.
                    cancelled = true;
                };

                while (!cancelled)
                {
                    consumer.Poll(TimeSpan.FromMilliseconds(100));
                }
            }
        }
    }
}
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace compound
{
    public class App
    {

        private readonly IProduserService _produserService;
        private readonly IConsumerService _consumerService;
        private readonly IConfigurationRoot _config;

        private readonly IEthereumService _ethereumService;

        private readonly ILogger<App> _logger;

        public App(ILogger<App> logger, IConfigurationRoot config, 
                    IConsumerService consumerService, 
                    IProduserService produserService,
                    IEthereumService ethereumService)
        {
            _produserService = produserService;
            _ethereumService = ethereumService;
            _consumerService = consumerService;
            _config = config;
            _logger = logger;
        }

        public async Task Run()
        {
            _logger.LogDebug("Test");
            await _produserService.Run();
            // await _ethereumService.GetBorrowTakenEvents(7000423, 7100423, 10);
            // await _produserService.Run();
            // await _consumerService.Run();

        }
    }
}
﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace compound
{
    class Program
    {
        public static IConfigurationRoot configuration;

        private static void Main(string[] args)
        {
            // Start!
            MainAsync().Wait();
        }
        private static async Task MainAsync()
        {

            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            // Create service provider
            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

            await serviceProvider.GetService<App>().Run();

            var block = new ActionBlock<string>(
                async x => await serviceProvider.GetService<App>().Run(),
                new ExecutionDataflowBlockOptions
                {
                    BoundedCapacity = 8, // Cap the item count
                    MaxDegreeOfParallelism = 10
                    //MaxDegreeOfParallelism = Environment.ProcessorCount, // Parallelize on all cores
                });

            block.Complete();
            await block.Completion;
        }
        private static void ConfigureServices(IServiceCollection serviceCollection)
        {

            // Add logging
            serviceCollection.AddSingleton(new LoggerFactory()
                .AddConsole()
                .AddSerilog()
                .AddDebug());
            serviceCollection.AddLogging();

            // Build configuration
            configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();


            // Add access to generic IConfigurationRoot
            serviceCollection.AddSingleton<IConfigurationRoot>(configuration);

            Log.Logger = new LoggerConfiguration()
                 .WriteTo.Console(Serilog.Events.LogEventLevel.Debug)
                 .MinimumLevel.Debug()
                 .Enrich.FromLogContext()
                 .CreateLogger();

            // Add services
            serviceCollection.AddTransient<IConsumerService, KafkaConsumerService>();
            serviceCollection.AddTransient<IProduserService, KafkaProduserService>();
            serviceCollection.AddTransient<IEthereumService, EthereumService>();

            // Add app
            serviceCollection.AddTransient<App>();
        }
    }
}
